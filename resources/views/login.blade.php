@extends('layout.header')
@section('title', 'Login | Laravel')
@section('content')
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        html,
        body {
            height: 100%;
        }

        body {
            display: flex;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: auto;
        }

        .form-signin .checkbox {
            font-weight: 400;
        }

        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }

        .form-signin .form-control:focus {
            z-index: 2;
            border-color: #dc3545;
            box-shadow: 0 0 0 0.25rem rgb(220 53 69 / 0.25)
        }

        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }

        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

    </style>

<body class="text-center">

    <main class="form-signin">
        <form action="/login" method="POST">
            @csrf
            <img class="mb-4" src="{{ asset('assets/img/laravel_logo.png') }}" alt="" height="57">
            <h1 class="h3 mb-3 fw-normal">Please sign in</h1>
            <label for="inputId" class="visually-hidden">ID</label>
            <input type="text" id="inputId" name="inputId" class="form-control" placeholder="ID" autofocus>
            <label for="inputPassword" class="visually-hidden">Password</label>
            <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password">
            <button class="w-100 btn btn-lg btn-danger" type="submit">Sign in</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2021</p>
        </form>
    </main>

</body>
@endsection
