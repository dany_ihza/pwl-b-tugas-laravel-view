<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $data['show'] = [
            'Nama' => 'Dany Ahmad Ihza Prakoso',
            'NIM' => '18150701111017',
            'Prodi' => 'Teknologi Informasi',
            'Kelas' => 'Pemrograman Web Lanjut - B'
        ];
        return view('welcome', $data);
    }

    public function login(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('login');
        } else {
            $request->validate([
                'inputId' => 'required',
                'inputPassword' => 'required',
            ]);
            if ($request->input('inputId') != '123' && $request->input('inputPassword') != '123') {
                return redirect('/login');
            }
            return redirect('/');
        }
    }
}
